package ai.maum.cloud.admin.auth.boundaries.dto

data class AppSignKeyDto(
        val parentApplication: String?,
        val signKeyOwnerEmail: String?,
        val locked: Boolean?,
        val scopeSet: Set<String>?
)