package ai.maum.cloud.admin.auth.infra.repository

import ai.maum.cloud.admin.auth.infra.entity.AccountEntity
import ai.maum.cloud.admin.auth.infra.entity.ApplicationEntity
import org.springframework.data.repository.CrudRepository

interface ApplicationRepository: CrudRepository<ApplicationEntity, Long> {
    fun findByApplicationName(applicationName: String): ApplicationEntity?
    fun existsByApplicationName(applicationName: String): Boolean
    fun findAllByOwnerAccount(accountEntity: AccountEntity): Set<ApplicationEntity>?
}