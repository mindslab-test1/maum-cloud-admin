package ai.maum.cloud.admin.auth.boundaries.controller

import ai.maum.cloud.admin.auth.boundaries.dto.AccountDto
import ai.maum.cloud.admin.auth.core.usecase.AuthUserUseCase
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(
        "auth"
)
class AuthUserController(
        private val authUserUseCase: AuthUserUseCase
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping(
            "getAccountDetails"
    )
    fun getAccountDetails(
            @RequestParam("email")
            email: String,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("get account details")
        val responseBody = authUserUseCase.getAccountDetails(accountEmail = email, authentication = oAuth2Authentication)
        return ResponseEntity.ok(responseBody)
    }

    @GetMapping(
            "getApplicationDetails"
    )
    fun getApplicationDetails(
            @RequestParam("applicationName")
            applicationName: String,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("get application details")
        val responseBody = authUserUseCase.getAppDetails(applicationName = applicationName, authentication = oAuth2Authentication)
        return ResponseEntity.ok(responseBody)
    }

    @GetMapping(
            "getSignKeyDetails"
    )
    fun getSignKeyDetails(
            @RequestParam("accountEmail")
            accountEmail: String,
            @RequestParam("applicationName")
            applicationName: String,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("get application details")
        val responseBody = authUserUseCase.getSignKeyDetails(
                accountEmail = accountEmail,
                applicationName = applicationName,
                authentication = oAuth2Authentication
        )
        return ResponseEntity.ok(responseBody)
    }

    @PutMapping(
            "account/create"
    )
    fun createAccount(
            @RequestBody
            accountDto: AccountDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("create account")
        val responseBody = authUserUseCase.createAccount(
                accountDto = accountDto,
                authentication = oAuth2Authentication
        )

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PostMapping(
            "account/update"
    )
    fun updateAccount(
            @RequestBody
            accountDto: AccountDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("update account")
        val responseBody = authUserUseCase.updateAccount(
                accountDto = accountDto,
                authentication = oAuth2Authentication
        )

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "account/lock"
    )
    fun lockAccount(
            @RequestBody
            accountDto: AccountDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("lock account")
        val responseBody = authUserUseCase.lockAccount(
                accountDto = accountDto,
                authentication = oAuth2Authentication
        )

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "account/unlock"
    )
    fun unlockAccount(
            @RequestBody
            accountDto: AccountDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("unlock account")
        val responseBody = authUserUseCase.unlockAccount(
                accountDto = accountDto,
                authentication = oAuth2Authentication
        )

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }
}