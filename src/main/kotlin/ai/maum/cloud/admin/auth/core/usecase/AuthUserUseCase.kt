package ai.maum.cloud.admin.auth.core.usecase

import ai.maum.cloud.admin.auth.boundaries.dto.AccountDto
import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto
import ai.maum.cloud.admin.auth.core.service.AuthUserService
import ai.maum.cloud.admin.utils.Constants
import org.slf4j.LoggerFactory
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.stereotype.Component

@Component
class AuthUserUseCase(
        private val jwtDecoder: JwtDecoder,
        private val authUserService: AuthUserService
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val parentClaims = setOf(
            Constants.CLAIM_CLOUD_AST,
            Constants.CLAIM_CLOUD_ACCOUNT_AST
    )

    fun getAccountDetails(accountEmail: String, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.getAccountDetails(accountEmail = accountEmail)
    }

    fun getAppDetails(applicationName: String, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.getApplicationDetails(applicationName = applicationName)
    }

    fun getSignKeyDetails(accountEmail: String, applicationName: String, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.getAppSignKeyDetails(accountEmail = accountEmail, applicationName = applicationName)
    }

    fun createAccount(accountDto: AccountDto, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.createAccount(accountDto)
    }

    fun updateAccount(accountDto: AccountDto, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.updateAccount(accountDto)
    }

    fun lockAccount(accountDto: AccountDto, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.lockAccount(accountDto)
    }

    fun unlockAccount(accountDto: AccountDto, authentication: OAuth2Authentication): ResponseBodyDto {
        this.validateClaim(authentication)
        return authUserService.unlockAccount(accountDto)
    }

    private fun validateClaim(authentication: OAuth2Authentication, specClaim: String? = null){
        val jwtTokenRaw = (authentication.details as? OAuth2AuthenticationDetails)?.tokenValue
                ?: TODO("throw exception")

        val claims = jwtDecoder.decode(jwtTokenRaw).claims["scope"] as List<*>

        logger.debug(claims.toString())
        var flag = false
        for (claim in parentClaims){
            if(claims.contains(claim)) {
                flag = true
                break
            }
        }
        if(!flag) flag = claims.contains(specClaim ?: TODO("throw exception"))
        if(!flag) TODO("throw exception")
    }
}