package ai.maum.cloud.admin.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "ROLE_ENTITY", schema = "API_AUTH")
@DynamicUpdate
class RoleEntity: BaseEntity() {
    @Id
    @SequenceGenerator(
            schema = "API_AUTH",
            name = "ROLE_SEQ_GEN",
            sequenceName = "ROLE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROLE_SEQ_GEN")
    var id: Long? = null

    @Column(unique = true, nullable = false)
    var roleName: String = ""

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    var validScopes: Set<ScopeEntity>? = null

    @ManyToMany(mappedBy = "roles")
    var apps: Set<ApplicationEntity>? = null
}