package ai.maum.cloud.admin.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "ACCOUNT_ENTITY", schema = "API_AUTH")
@DynamicUpdate
class AccountEntity : BaseEntity(){
    @Id
    @SequenceGenerator(
            schema = "API_AUTH",
            name = "ACCOUNT_SEQ_GEN",
            sequenceName = "ACCOUNT_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCOUNT_SEQ_GEN")
    var id: Long? = null

    @Column(unique = true, nullable = false)
    var email: String = ""

    @Column(nullable = false)
    var username: String = ""

    @Column(nullable = false)
    var passwordHashed: String = ""

    var locked: Boolean = false
}
