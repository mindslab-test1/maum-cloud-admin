package ai.maum.cloud.admin.auth.infra.impl

import ai.maum.cloud.admin.auth.boundaries.dto.AccountDto
import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto
import ai.maum.cloud.admin.auth.boundaries.dto.SignKeyInfo
import ai.maum.cloud.admin.auth.core.service.AuthUserService
import ai.maum.cloud.admin.auth.infra.entity.AccountEntity
import ai.maum.cloud.admin.auth.infra.repository.AccountRepository
import ai.maum.cloud.admin.auth.infra.repository.AppSignKeyRepository
import ai.maum.cloud.admin.auth.infra.repository.ApplicationRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class AuthUserServiceImpl(
        private val accountRepository: AccountRepository,
        private val applicationRepository: ApplicationRepository,
        private val appSignKeyRepository: AppSignKeyRepository
): AuthUserService {
    override fun getAccountDetails(accountEmail: String): ResponseBodyDto {
        val account = accountRepository.findTopByEmail(email = accountEmail)
                ?: TODO("throw exception")
        val payload = mutableMapOf<String, Any?>()
        payload["email"] = account.email
        payload["username"] = account.username
        payload["locked"] = account.locked
        payload["created"] = account.createdAt
        payload["updated"] = account.updatedAt

        val applications = applicationRepository.findAllByOwnerAccount(accountEntity = account)
        val appSet = mutableSetOf<String>()
        applications?.forEach { application ->
            appSet.add(application.applicationName)
        }
        payload["applications"] = appSet

        return ResponseBodyDto(
                payload = payload
        )
    }

    override fun getApplicationDetails(applicationName: String): ResponseBodyDto {
        val application = applicationRepository.findByApplicationName(applicationName = applicationName)
                ?: TODO("throw exception")
        val payload = mutableMapOf<String, Any?>()
        payload["applicationName"] = application.applicationName
        payload["ownerEmail"] = application.ownerAccount?.email ?: "NOT_FOUND"
        payload["locked"] = application.locked

        val roleSet = mutableSetOf<String>()
        application.roles?.forEach { roleEntity ->
            roleSet.add(roleEntity.roleName)
        }
        payload["roles"] = roleSet

        val signKeySet = mutableSetOf<SignKeyInfo>()
        val issuedSignKeys = appSignKeyRepository.findAllByApplication(application = application)
        issuedSignKeys?.forEach { signKey ->
            val scopeSet = mutableSetOf<String>()
            signKey.scopes?.forEach{ scope ->
                scopeSet.add(scope.scopeName ?: "NOT_FOUND")
            }

            signKeySet.add(SignKeyInfo(
                    email = signKey.keyOwner?.email,
                    scopes = scopeSet
            ))
        }
        payload["issuedKeys"] = signKeySet

        return ResponseBodyDto(
                payload = payload
        )
    }

    override fun getAppSignKeyDetails(accountEmail: String, applicationName: String): ResponseBodyDto {
        val application = applicationRepository.findByApplicationName(applicationName = applicationName)
                ?: TODO("throw exception")
        val keyOwner = accountRepository.findTopByEmail(email = accountEmail)
                ?: TODO("throw exception")

        val appSignKey = appSignKeyRepository.findByApplicationAndKeyOwner(
                application = application,
                keyOwner = keyOwner
        ) ?: TODO("throw exception")

        val payload = mutableMapOf<String, Any?>()
        payload["applicationName"] = application.applicationName
        payload["applicationOwner"] = application.ownerAccount?.email ?: "NOT_FOUND"
        payload["ownerEmail"] = keyOwner.email
        payload["clientKey"] = appSignKey.clientKey
        payload["clientSecret"] = appSignKey.clientSecret.split("}")[1]

        val scopes = mutableSetOf<String>()
        appSignKey.scopes?.forEach { scope ->
            scopes.add(scope.scopeName ?: "NOT_FOUND")
        }
        payload["validScopes"] = scopes

        return ResponseBodyDto(
                payload = payload
        )
    }

    override fun createAccount(accountDto: AccountDto): ResponseBodyDto {
        if(accountRepository.existsByEmail(accountDto.email) ?: TODO("throw exception"))
            TODO("throw exception")

        val newAccount = AccountEntity()
        newAccount.email = accountDto.email
        newAccount.username = accountDto.username ?: accountDto.email.split("@")[0]
        newAccount.passwordHashed = accountDto.passwordHashed ?: "NOT_PROVIDED"
        val resAccount = accountRepository.save(newAccount)
        val payload = mutableMapOf<String, Any?>()
        payload["email"] = resAccount.email
        payload["username"] = resAccount.username
        payload["locked"] = resAccount.locked

        return ResponseBodyDto(
                status = HttpStatus.CREATED.value(),
                payload = payload
        )
    }

    override fun updateAccount(accountDto: AccountDto): ResponseBodyDto {
        val prevAccount = accountRepository.findTopByEmail(accountDto.email)
                ?: TODO("throw exception")

        prevAccount.username = accountDto.username ?: prevAccount.username
        prevAccount.passwordHashed = accountDto.passwordHashed ?: prevAccount.passwordHashed
        prevAccount.locked = accountDto.locked ?: prevAccount.locked
        val resAccount = accountRepository.save(prevAccount)
        val payload = mutableMapOf<String, Any?>()
        payload["email"] = resAccount.email
        payload["username"] = resAccount.username
        payload["locked"] = resAccount.locked

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value(),
                payload = payload
        )
    }

    override fun lockAccount(accountDto: AccountDto): ResponseBodyDto {
        val targetAccount = accountRepository.findTopByEmail(accountDto.email)
                ?: TODO("throw exception")
        val applications = applicationRepository.findAllByOwnerAccount(targetAccount)
        applications?.forEach { application ->
            application.locked = true
            applicationRepository.save(application)
        }

        targetAccount.locked = true
        accountRepository.save(targetAccount)

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun unlockAccount(accountDto: AccountDto): ResponseBodyDto {
        val targetAccount = accountRepository.findTopByEmail(accountDto.email)
                ?: TODO("throw exception")

        if(!targetAccount.locked) TODO("throw exception")

        targetAccount.locked = false
        val resAccount = accountRepository.save(targetAccount)
        val payload = mutableMapOf<String, Any?>()
        payload["email"] = resAccount.email
        payload["username"] = resAccount.username
        payload["locked"] = resAccount.locked

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value(),
                payload = payload
        )
    }
}