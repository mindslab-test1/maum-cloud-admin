package ai.maum.cloud.admin.auth.boundaries.dto

data class SignKeyInfo(
        val email: String?,
        val scopes: Set<String>?,
)