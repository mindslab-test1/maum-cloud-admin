package ai.maum.cloud.admin.auth.boundaries.dto

data class ApplicationDto(
        val applicationName: String,
        val applicationOwnerEmail: String?,
        val locked: Boolean?,
        val roleSet: Set<String>?
)