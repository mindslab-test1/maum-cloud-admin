package ai.maum.cloud.admin.auth.infra.impl

import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto
import ai.maum.cloud.admin.auth.boundaries.dto.RoleDto
import ai.maum.cloud.admin.auth.boundaries.dto.ScopeDto
import ai.maum.cloud.admin.auth.core.service.AuthEntityService
import org.springframework.stereotype.Service

@Service
class AuthEntityServiceImpl(

): AuthEntityService {
    override fun createRole(roleDto: RoleDto): ResponseBodyDto {
        TODO("Not yet implemented")
    }

    override fun removeRole(roleDto: RoleDto): ResponseBodyDto {
        TODO("Not yet implemented")
    }

    override fun createScopes(scopeDto: ScopeDto): ResponseBodyDto {
        TODO("Not yet implemented")
    }

    override fun removeScopes(scopeDto: ScopeDto): ResponseBodyDto {
        TODO("Not yet implemented")
    }
}