package ai.maum.cloud.admin.auth.boundaries.dto

data class AccountDto(
        val email: String,
        val username: String?,
        val passwordHashed: String? = "",
        val locked: Boolean? = false
)