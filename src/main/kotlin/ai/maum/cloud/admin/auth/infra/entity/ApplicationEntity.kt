package ai.maum.cloud.admin.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "APPLICATION_ENTITY", schema = "API_AUTH")
@DynamicUpdate
class ApplicationEntity : BaseEntity(){
    @Id
    @SequenceGenerator(
            schema = "API_AUTH",
            name = "APP_SEQ_GEN",
            sequenceName = "APP_SEQ",
            initialValue = 1,
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_SEQ_GEN")
    var id: Long? = null

    @Column
    var applicationName: String = ""

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_account_id", referencedColumnName = "id")
    var ownerAccount: AccountEntity? = null

    @Column
    var locked: Boolean = false

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "APP_ROLES",
            schema = "API_AUTH",
            joinColumns = [JoinColumn(name = "app_id")],
            inverseJoinColumns = [JoinColumn(name = "role_id")]
    )
    var roles: Set<RoleEntity>? = null
}