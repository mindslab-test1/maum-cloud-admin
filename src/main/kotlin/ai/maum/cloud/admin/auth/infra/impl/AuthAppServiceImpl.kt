package ai.maum.cloud.admin.auth.infra.impl

import ai.maum.cloud.admin.auth.boundaries.dto.AppSignKeyDto
import ai.maum.cloud.admin.auth.boundaries.dto.ApplicationDto
import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto
import ai.maum.cloud.admin.auth.core.service.AuthAppService
import ai.maum.cloud.admin.auth.infra.entity.*
import ai.maum.cloud.admin.auth.infra.repository.*
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import java.util.*

@Service
class AuthAppServiceImpl(
        private val accountRepository: AccountRepository,
        private val applicationRepository: ApplicationRepository,
        private val roleRepository: RoleRepository,
        private val scopeRepository: ScopeRepository,
        private val appSignKeyRepository: AppSignKeyRepository,
): AuthAppService {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun createApplication(applicationDto: ApplicationDto): ResponseBodyDto {
        logger.debug("find creator account: ${applicationDto.applicationOwnerEmail}")
        val creatorAccount = applicationDto.applicationOwnerEmail?.let { accountRepository.findTopByEmail(it) }
                ?: TODO("throw exception")
        logger.debug("find duplicate application name: ${applicationDto.applicationName}")
        if(applicationRepository.existsByApplicationName(applicationDto.applicationName))
            TODO("throw exception")

        logger.debug("add specified roles")
        val roleEntities = mutableSetOf<RoleEntity>()
        applicationDto.roleSet?.forEach { role ->
            logger.debug("for role name: $role")
            roleRepository.findByRoleName(role)?.let { entity ->
                logger.debug("role entity found: ${entity.roleName}")
                roleEntities.add(entity)
            }
        }

        logger.debug("create new application entity")
        val newApplication = ApplicationEntity()
        newApplication.applicationName = applicationDto.applicationName
        newApplication.ownerAccount = creatorAccount
        newApplication.locked = false
        newApplication.roles = roleEntities
        val resApplication = applicationRepository.save(newApplication)

        logger.debug("create default sign key")
        val newSignKey = AppSignKeyEntity()
        newSignKey.application = resApplication
        newSignKey.clientKey = UUID.randomUUID().toString()
        newSignKey.clientSecret = "{noop}${UUID.randomUUID()}"
        newSignKey.keyOwner = creatorAccount

        logger.debug("add all scopes for all roles specified")
        val scopeSet = mutableSetOf<ScopeEntity>()
        resApplication.roles?.forEach { roleEntity ->
            scopeRepository.findByAssignedRole(roleEntity)?.let { scopeEntityList ->
                scopeEntityList.forEach { scopeEntity ->
                    scopeSet.add(scopeEntity)
                }
            }
        }
        newSignKey.scopes = scopeSet
        newSignKey.locked = false
        newSignKey.accessTokenValiditySeconds = 1209600
        newSignKey.refreshTokenValiditySeconds = 2592000
        val resultSignKey = appSignKeyRepository.save(newSignKey)

        logger.debug("create payload")
        val payload = mutableMapOf<String, Any?>()
        payload["applicationName"] = resultSignKey.application?.applicationName
                ?: applicationDto.applicationName
        payload["applicationOwner"] = resultSignKey.keyOwner?.email
                ?: applicationDto.applicationOwnerEmail
        payload["defaultKey"] = resultSignKey.clientKey
        payload["defaultSecret"] = resultSignKey.clientSecret.split("}")[1]

        logger.debug("send response")
        return ResponseBodyDto(
                status = HttpStatus.CREATED.value(),
                payload = payload
        )
    }

    override fun lockApplication(applicationDto: ApplicationDto): ResponseBodyDto {
        return this.lockOrUnlockApplication(applicationDto, true)
    }

    override fun unlockApplication(applicationDto: ApplicationDto): ResponseBodyDto {
        return this.lockOrUnlockApplication(applicationDto, false)
    }

    private fun lockOrUnlockApplication(applicationDto: ApplicationDto, lock: Boolean): ResponseBodyDto {
        logger.debug("find target application: ${applicationDto.applicationName}")
        val targetApplication = applicationRepository.findByApplicationName(applicationName = applicationDto.applicationName)
                ?: TODO("throw exception")

        logger.debug("unlock application")
        targetApplication.locked = lock
        applicationRepository.save(targetApplication)

        logger.debug("send resposne")
        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun addRoleToApp(applicationDto: ApplicationDto): ResponseBodyDto {
        logger.debug("find target application: ${applicationDto.applicationName}")
        val targetApplication = applicationRepository.findByApplicationName(applicationName = applicationDto.applicationName)
                ?: TODO("throw exception")

        logger.debug("verify roles to be added")
        val newRoleSet = mutableSetOf<RoleEntity>()
        val currentRoleNames = mutableSetOf<String>()
        targetApplication.roles?.forEach { role ->
            currentRoleNames.add(role.roleName)
        }
        applicationDto.roleSet?.forEach { roleName ->
            if(!currentRoleNames.contains(roleName)) {
                roleRepository.findByRoleName(roleName)?.let { roleEntity ->
                    newRoleSet.add(roleEntity)
                }
            }
        }
        logger.debug("add previous roles")
        targetApplication.roles?.let { prevRoles ->
            newRoleSet.addAll(prevRoles)
        }

        logger.debug("save new roles")
        targetApplication.roles = newRoleSet

        applicationRepository.save(targetApplication)

        logger.debug("send response")
        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun removeRoleFromApp(applicationDto: ApplicationDto): ResponseBodyDto {
        logger.debug("find target application: ${applicationDto.applicationName}")
        val targetApplication = applicationRepository.findByApplicationName(applicationName = applicationDto.applicationName)
                ?: TODO("throw exception")

        logger.debug("remove specified roles")
        (targetApplication.roles as MutableSet).removeIf { roleEntity ->
            applicationDto.roleSet?.contains(roleEntity.roleName) ?: false
        }

        logger.debug("save application")
        applicationRepository.save(targetApplication)

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun createAppSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        logger.debug("find target application: ${appSignKeyDto.parentApplication}")
        val targetApplication = appSignKeyDto.parentApplication?.let { applicationRepository.findByApplicationName(applicationName = it) }
                ?: TODO("throw exception")
        logger.debug("find target account: ${appSignKeyDto.signKeyOwnerEmail}")
        val targetAccount = appSignKeyDto.signKeyOwnerEmail?.let { accountRepository.findTopByEmail(email = it) }
                ?: TODO("throw exception")

        logger.debug("create new sign key")
        val newSignKey = AppSignKeyEntity()
        newSignKey.application = targetApplication
        newSignKey.clientKey = UUID.randomUUID().toString()
        newSignKey.clientSecret = "{noop}${UUID.randomUUID()}"
        newSignKey.keyOwner = targetAccount

        logger.debug("validate sign key scopes")
        val givenScopes = mutableSetOf<ScopeEntity>()
        targetApplication.roles?.forEach{ roleEntity ->
            roleEntity.validScopes?.forEach { scopeEntity ->
                if(appSignKeyDto.scopeSet?.contains(scopeEntity.scopeName) == true)
                    givenScopes.add(scopeEntity)
            }
        }

        newSignKey.scopes = givenScopes
        newSignKey.locked = false
        newSignKey.accessTokenValiditySeconds = 1209600
        newSignKey.refreshTokenValiditySeconds = 2592000
        val resultSignKey = appSignKeyRepository.save(newSignKey)

        logger.debug("create payload")
        val payload = mutableMapOf<String, Any?>()
        payload["applicationName"] = resultSignKey.application?.applicationName
                ?: appSignKeyDto.parentApplication
        payload["applicationOwner"] = resultSignKey.keyOwner?.email
                ?: appSignKeyDto.signKeyOwnerEmail
        payload["defaultKey"] = resultSignKey.clientKey
        payload["defaultSecret"] = resultSignKey.clientSecret.split("}")[1]

        val givenScopeResult = mutableSetOf<String>()
        resultSignKey.scopes?.forEach{ scopeEntity ->
            scopeEntity.scopeName?.let { givenScopeResult.add(it) }
        }
        payload["givenScopes"] = givenScopeResult

        logger.debug("send response")
        return ResponseBodyDto(
                status = HttpStatus.CREATED.value(),
                payload = payload
        )
    }

    override fun lockSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        return this.lockOrUnlockSignKey(appSignKeyDto, true)
    }

    override fun unlockSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        return this.lockOrUnlockSignKey(appSignKeyDto, false)
    }

    private fun lockOrUnlockSignKey(appSignKeyDto: AppSignKeyDto, lock: Boolean): ResponseBodyDto{
        logger.debug("find target application: ${appSignKeyDto.parentApplication}")
        val targetApplication = appSignKeyDto.parentApplication?.let { applicationRepository.findByApplicationName(applicationName = it) }
                ?: TODO("throw exception")
        logger.debug("find target account: ${appSignKeyDto.signKeyOwnerEmail}")
        val targetAccount = appSignKeyDto.signKeyOwnerEmail?.let { accountRepository.findTopByEmail(email = it) }
                ?: TODO("throw exception")

        logger.debug("find target sign key")
        val targetSignKey = appSignKeyRepository.findByApplicationAndKeyOwner(
                application = targetApplication,
                keyOwner = targetAccount
        ) ?: TODO("throw exception")

        logger.debug("set sign key lock state: $lock")
        targetSignKey.locked = lock
        appSignKeyRepository.save(targetSignKey)

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun addScopeToSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        logger.debug("find target application: ${appSignKeyDto.parentApplication}")
        val targetApplication = appSignKeyDto.parentApplication?.let { applicationRepository.findByApplicationName(applicationName = it) }
                ?: TODO("throw exception")
        logger.debug("find target account: ${appSignKeyDto.signKeyOwnerEmail}")
        val targetAccount = appSignKeyDto.signKeyOwnerEmail?.let { accountRepository.findTopByEmail(email = it) }
                ?: TODO("throw exception")

        val targetSignKey = appSignKeyRepository.findByApplicationAndKeyOwner(
                application = targetApplication,
                keyOwner = targetAccount
        ) ?: TODO("throw exception")

        logger.debug("verify scopes to add")
        val scopesToAdd = mutableSetOf<ScopeEntity>()
        targetSignKey.application?.roles?.forEach { roleEntity ->
            roleEntity.validScopes?.forEach { scopeEntity ->
                if(appSignKeyDto.scopeSet?.contains(scopeEntity.scopeName) == true)
                    scopesToAdd.add(scopeEntity)
            }
        }
        (targetSignKey.scopes as MutableSet).addAll(scopesToAdd)

        logger.debug("save sign key")
        appSignKeyRepository.save(targetSignKey)

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun removeScopeFromSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        logger.debug("find target application: ${appSignKeyDto.parentApplication}")
        val targetApplication = appSignKeyDto.parentApplication?.let { applicationRepository.findByApplicationName(applicationName = it) }
                ?: TODO("throw exception")
        logger.debug("find target account: ${appSignKeyDto.signKeyOwnerEmail}")
        val targetAccount = appSignKeyDto.signKeyOwnerEmail?.let { accountRepository.findTopByEmail(email = it) }
                ?: TODO("throw exception")

        val targetSignKey = appSignKeyRepository.findByApplicationAndKeyOwner(
                application = targetApplication,
                keyOwner = targetAccount
        ) ?: TODO("throw exception")

        logger.debug("remove scopes from sign key")
        (targetSignKey.scopes as MutableSet).removeIf { scopeEntity ->
            appSignKeyDto.scopeSet?.contains(scopeEntity.scopeName) ?: false
        }

        logger.debug("save sign key")
        appSignKeyRepository.save(targetSignKey)

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value()
        )
    }

    override fun refreshKeySecret(appSignKeyDto: AppSignKeyDto): ResponseBodyDto {
        logger.debug("find target application: ${appSignKeyDto.parentApplication}")
        val targetApplication = appSignKeyDto.parentApplication?.let { applicationRepository.findByApplicationName(applicationName = it) }
                ?: TODO("throw exception")
        logger.debug("find target account: ${appSignKeyDto.signKeyOwnerEmail}")
        val targetAccount = appSignKeyDto.signKeyOwnerEmail?.let { accountRepository.findTopByEmail(email = it) }
                ?: TODO("throw exception")

        val targetSignKey = appSignKeyRepository.findByApplicationAndKeyOwner(
                application = targetApplication,
                keyOwner = targetAccount
        ) ?: TODO("throw exception")

        logger.debug("give new sign key")
        targetSignKey.clientKey = UUID.randomUUID().toString()
        targetSignKey.clientSecret = "{noop}${UUID.randomUUID()}"
        val resSignKey = appSignKeyRepository.save(targetSignKey)

        logger.debug("create payload")
        val payload = mutableMapOf<String, Any>()
        payload["newKey"] = resSignKey.clientKey
        payload["newSecret"] = resSignKey.clientSecret.split("}")[1]

        return ResponseBodyDto(
                status = HttpStatus.ACCEPTED.value(),
                payload = payload
        )
    }
}