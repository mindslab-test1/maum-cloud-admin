package ai.maum.cloud.admin.auth.core.service

import ai.maum.cloud.admin.auth.boundaries.dto.AppSignKeyDto
import ai.maum.cloud.admin.auth.boundaries.dto.ApplicationDto
import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto

interface AuthAppService {
    fun createApplication(applicationDto: ApplicationDto): ResponseBodyDto
    fun lockApplication(applicationDto: ApplicationDto): ResponseBodyDto
    fun unlockApplication(applicationDto: ApplicationDto): ResponseBodyDto

    fun addRoleToApp(applicationDto: ApplicationDto): ResponseBodyDto
    fun removeRoleFromApp(applicationDto: ApplicationDto): ResponseBodyDto

    fun createAppSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto
    fun lockSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto
    fun unlockSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto

    fun addScopeToSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto
    fun removeScopeFromSignKey(appSignKeyDto: AppSignKeyDto): ResponseBodyDto

    fun refreshKeySecret(appSignKeyDto: AppSignKeyDto): ResponseBodyDto
}