package ai.maum.cloud.admin.auth.core.service

import ai.maum.cloud.admin.auth.boundaries.dto.AccountDto
import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto

interface AuthUserService {
    fun getAccountDetails(accountEmail: String): ResponseBodyDto
    fun getApplicationDetails(applicationName: String): ResponseBodyDto
    fun getAppSignKeyDetails(accountEmail: String, applicationName: String): ResponseBodyDto

    fun createAccount(accountDto: AccountDto): ResponseBodyDto
    fun updateAccount(accountDto: AccountDto): ResponseBodyDto
    fun lockAccount(accountDto: AccountDto): ResponseBodyDto
    fun unlockAccount(accountDto: AccountDto): ResponseBodyDto
}