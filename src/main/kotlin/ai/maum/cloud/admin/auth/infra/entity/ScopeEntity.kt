package ai.maum.cloud.admin.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "SCOPE_ENTITY", schema = "API_AUTH")
@DynamicUpdate
class ScopeEntity: BaseEntity() {
    @Id
    @SequenceGenerator(
            schema = "API_AUTH",
            name = "SCOPE_SEQ_GEN",
            sequenceName = "SCOPE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SCOPE_SEQ_GEN")
    var id: Long? = null

    @Column(unique = true, nullable = false)
    var scopeName: String? = null

    @ManyToMany(mappedBy = "scopes")
    var signKeys: Set<AppSignKeyEntity>? = null

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    var assignedRole: RoleEntity? = null
}