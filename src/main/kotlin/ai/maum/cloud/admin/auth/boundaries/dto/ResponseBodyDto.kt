package ai.maum.cloud.admin.auth.boundaries.dto

data class ResponseBodyDto(
        val status: Int = 200,
        val message: String? = "Success",
        var payload: Map<String, Any?>? = null,
)