package ai.maum.cloud.admin.auth.infra.entity

import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "APP_SIGN_KEY_ENTITY", schema = "API_AUTH")
class AppSignKeyEntity: BaseEntity() {
    @Id
    @SequenceGenerator(
            schema = "API_AUTH",
            name = "APP_REF_SEQ_GEN",
            sequenceName = "APP_REF_SEQ",
            initialValue = 1,
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_REF_SEQ_GEN")
    var id: Long? = null

    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "app_id", referencedColumnName = "id")
    var application: ApplicationEntity? = null

    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "key_owner_id", referencedColumnName = "id")
    var keyOwner: AccountEntity? = null

    @Column(unique = true)
    var clientKey: String = ""

    @Column(unique = true)
    var clientSecret: String = ""

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "APP_KEY_SCOPES",
            schema = "API_AUTH",
            joinColumns = [JoinColumn(name = "sign_key_id")],
            inverseJoinColumns = [JoinColumn(name = "scope_id")]
    )
    var scopes: Set<ScopeEntity>? = null

    @Column
    var accessTokenValiditySeconds: Int? = null

    @Column
    var refreshTokenValiditySeconds: Int? = null

    @Column
    var locked: Boolean = false
}

