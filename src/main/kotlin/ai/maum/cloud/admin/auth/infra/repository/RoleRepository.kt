package ai.maum.cloud.admin.auth.infra.repository

import ai.maum.cloud.admin.auth.infra.entity.RoleEntity
import org.springframework.data.repository.CrudRepository

interface RoleRepository: CrudRepository<RoleEntity, Long> {
    fun findByRoleName(roleName: String): RoleEntity?
    fun existsByRoleName(roleName: String): Boolean
}