package ai.maum.cloud.admin.auth.infra.repository

import ai.maum.cloud.admin.auth.infra.entity.AccountEntity
import org.springframework.data.repository.CrudRepository

interface AccountRepository: CrudRepository<AccountEntity, Long> {
    fun existsByEmail(email: String): Boolean?

    fun findTopByEmail(email: String): AccountEntity?

    fun findTopByOrderById(): AccountEntity?
}