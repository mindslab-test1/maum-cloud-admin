package ai.maum.cloud.admin.auth.boundaries.dto

data class RoleDto(
        val roleName: String
)