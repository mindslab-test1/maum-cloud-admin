package ai.maum.cloud.admin.auth.infra.repository

import ai.maum.cloud.admin.auth.infra.entity.AccountEntity
import ai.maum.cloud.admin.auth.infra.entity.AppSignKeyEntity
import ai.maum.cloud.admin.auth.infra.entity.ApplicationEntity
import org.springframework.data.repository.CrudRepository

interface AppSignKeyRepository: CrudRepository<AppSignKeyEntity, Long> {
    fun findAllByApplication(application: ApplicationEntity): Set<AppSignKeyEntity>?
    fun findByApplicationAndKeyOwner(application: ApplicationEntity, keyOwner: AccountEntity): AppSignKeyEntity?
}