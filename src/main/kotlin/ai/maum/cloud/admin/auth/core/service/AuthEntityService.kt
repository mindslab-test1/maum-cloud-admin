package ai.maum.cloud.admin.auth.core.service

import ai.maum.cloud.admin.auth.boundaries.dto.ResponseBodyDto
import ai.maum.cloud.admin.auth.boundaries.dto.RoleDto
import ai.maum.cloud.admin.auth.boundaries.dto.ScopeDto

interface AuthEntityService {
    fun createRole(roleDto: RoleDto): ResponseBodyDto
    fun removeRole(roleDto: RoleDto): ResponseBodyDto

    fun createScopes(scopeDto: ScopeDto): ResponseBodyDto
    fun removeScopes(scopeDto: ScopeDto): ResponseBodyDto
}