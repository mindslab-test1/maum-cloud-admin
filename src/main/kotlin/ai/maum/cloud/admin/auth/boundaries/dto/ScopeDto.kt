package ai.maum.cloud.admin.auth.boundaries.dto

data class ScopeDto(
        val role: String,
        val scopes: Set<String>
)