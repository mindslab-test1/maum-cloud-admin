package ai.maum.cloud.admin.auth.boundaries.controller

import ai.maum.cloud.admin.auth.boundaries.dto.AccountDto
import ai.maum.cloud.admin.auth.boundaries.dto.AppSignKeyDto
import ai.maum.cloud.admin.auth.boundaries.dto.ApplicationDto
import ai.maum.cloud.admin.auth.core.service.AuthAppService
import ai.maum.cloud.admin.auth.core.usecase.AuthUserUseCase
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(
        "auth"
)
class AuthAppController(
        private val authAppService: AuthAppService
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)


    @PutMapping(
            "application/create"
    )
    fun createApplication(
            @RequestBody
            applicationDto: ApplicationDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("create application")
        val responseBody = authAppService.createApplication(applicationDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "application/lock"
    )
    fun lockApplication(
            @RequestBody
            applicationDto: ApplicationDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("lock application")
        val responseBody = authAppService.lockApplication(applicationDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "application/unlock"
    )
    fun unlockApplication(
            @RequestBody
            applicationDto: ApplicationDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("unlock application")
        val responseBody = authAppService.unlockApplication(applicationDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PutMapping(
            "application/role/add"
    )
    fun addRoleToApplication(
            @RequestBody
            applicationDto: ApplicationDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("add role to application")
        val responseBody = authAppService.addRoleToApp(applicationDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "application/role/remove"
    )
    fun removeRoleFromApplication(
            @RequestBody
            applicationDto: ApplicationDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("remove role to application")
        val responseBody = authAppService.removeRoleFromApp(applicationDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PutMapping(
            "appKey/create"
    )
    fun createSignKey(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("create app sign key")
        val responseBody = authAppService.createAppSignKey(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "appKey/lock"
    )
    fun lockSignKey(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("lock app sign key")
        val responseBody = authAppService.lockSignKey(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "appKey/unlock"
    )
    fun unlockSignKey(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("unlock app sign key")
        val responseBody = authAppService.unlockSignKey(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PutMapping(
            "appKey/addScope"
    )
    fun addScopeToKey(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("add scope to key")
        val responseBody = authAppService.addScopeToSignKey(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "appKey/removeScope"
    )
    fun removeScopeFromKey(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("add scope to key")
        val responseBody = authAppService.removeScopeFromSignKey(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }

    @PatchMapping(
            "appKey/refreshKeySecret"
    )
    fun refreshKeySecret(
            @RequestBody
            appSignKeyDto: AppSignKeyDto,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any> {
        logger.info("add scope to key")
        val responseBody = authAppService.refreshKeySecret(appSignKeyDto)

        return ResponseEntity.status(responseBody.status)
                .body(responseBody)
    }
}