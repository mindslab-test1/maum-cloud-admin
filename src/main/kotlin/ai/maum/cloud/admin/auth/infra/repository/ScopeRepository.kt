package ai.maum.cloud.admin.auth.infra.repository

import ai.maum.cloud.admin.auth.infra.entity.RoleEntity
import ai.maum.cloud.admin.auth.infra.entity.ScopeEntity
import org.springframework.data.repository.CrudRepository

interface ScopeRepository: CrudRepository<ScopeEntity, Long> {
    fun findByAssignedRole(assignedRole: RoleEntity): List<ScopeEntity>?
}