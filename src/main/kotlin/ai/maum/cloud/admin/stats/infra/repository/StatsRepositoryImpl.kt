package ai.maum.cloud.admin.stats.infra.repository

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository

@Repository
class StatsRepositoryImpl(
        private val apiUsageRepository: ApiUsageRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    fun test() {
        val topAccount = apiUsageRepository.findTopByOrderById()
        logger.info(topAccount.toString())
    }
}