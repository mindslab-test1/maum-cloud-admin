package ai.maum.cloud.admin.stats.infra.repository

import ai.maum.cloud.admin.stats.infra.entity.ApiUsageEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(readOnly = true)
interface ApiUsageRepository: CrudRepository<ApiUsageEntity, Long> {
    fun findTopByOrderById(): ApiUsageEntity?
}