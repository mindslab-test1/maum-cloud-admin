package ai.maum.cloud.admin.stats.boundaries.controller

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(
        "admin/stats"
)
class StatsApiController {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    /**
     * read RDB statistics based on parameters.
     * 파라미터가 많아서 POST로...나중에 이것도 jwt로 파라미터 주고 받든가 해서 REST에 맞게 GET으로 바꾸거나 해야할듯
     *
     * TODO
     * @param   account (optional)
     * @param   applicationName (optional)
     * @param   service (optional) (Set)
     * @param   pathAbsolute (optional) (Set)
     * @param   success (optional)
     * @param   startDate (optional) (default is -1 month of current date)
     * @param   endDate (optional) (default is now)
     */
    @PostMapping(
            "rdb/read"
    )
    fun getRdbStats(){
    }

    /**
     * read detail statistics, including MongoDB data.
     *
     * TODO
     * @param   requestUUID
     */
    @GetMapping(
            "mongo/detail"
    )
    fun getMongoStats(){

    }
}