package ai.maum.cloud.admin.bondaries

import ai.maum.cloud.admin.auth.infra.repository.*
import ai.maum.cloud.admin.stats.infra.repository.StatsRepositoryImpl
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping(
        "test"
)
class TestController(
        val jwtDecoder: JwtDecoder,
        val accountRepository: AccountRepository,
        val applicationRepository: ApplicationRepository,
        val appSignKeyRepository: AppSignKeyRepository,
        val roleRepository: RoleRepository,
        val scopeRepository: ScopeRepository,
        val statsRepositoryImpl: StatsRepositoryImpl
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping(
            "all"
    )
    fun testAll(
//            jwt: JwtAuthenticationToken,
            oAuth2Authentication: OAuth2Authentication
    ): ResponseEntity<Any>{
        val tokenValue = (oAuth2Authentication.details as? OAuth2AuthenticationDetails)?.tokenValue
        val jwtMap = jwtDecoder.decode(tokenValue)
        jwtMap.claims.forEach { claimEntry ->
            logger.info("claim key: ${claimEntry.key}")
            logger.info("claim value: ${claimEntry.value}")
        }

        logger.info(accountRepository.findById(1).toString())
        logger.info(applicationRepository.findById(1).toString())
        logger.info(appSignKeyRepository.findById(1).toString())
        logger.info(roleRepository.findById(1).toString())
        logger.info(scopeRepository.findById(1).toString())
        statsRepositoryImpl.test()
        return ResponseEntity.noContent().build()
    }
}