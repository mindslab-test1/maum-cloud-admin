package ai.maum.cloud.admin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CloudAdminApplication

fun main(args: Array<String>) {
	runApplication<CloudAdminApplication>(*args)
}
