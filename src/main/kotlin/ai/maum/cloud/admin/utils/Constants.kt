package ai.maum.cloud.admin.utils

object Constants {
    const val CLAIM_CLOUD_AST = "ai-maum-cloud-*"
    const val CLAIM_CLOUD_ACCOUNT_AST = "ai-maum-cloud-account-*"
    const val CLAIM_CLOUD_ROLE_AST = "ai-maum-cloud-role-*"
    const val CLAIM_CLOUD_SCOPE_AST = "ai-maum-cloud-scope-*"
    const val CLAIM_CLOUD_APP_AST = "ai-maum-cloud-application-*"
    const val CLAIM_CLOUD_APPKEY_AST = "ai-maum-cloud-app_key-*"
}