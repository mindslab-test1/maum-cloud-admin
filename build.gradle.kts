import com.google.protobuf.gradle.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// gRPC dependencies
val grpcVersion = "1.33.0"
val grpcKotlinVersion = "0.2.1"
val grpcKotlinExt = "jdk7@jar"
val protobufVersion = "3.14.0"

// gRPC directory configurations
val grpcGeneratedDir = ".grpc_generated"
val grpcOutputSubDir = "grpc"
val grpcKtOutputSubDir = "grpckt"

// Kotlinx dependencies
val serializationVersion = "1.0.1"
val coroutinesVersion = "1.4.1"

plugins {
	application
	id("org.springframework.boot") version "2.4.0"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	kotlin("jvm") version "1.4.10"
	kotlin("plugin.spring") version "1.4.10"
	kotlin("plugin.jpa") version "1.4.10"
	kotlin("plugin.serialization") version "1.4.10"
	id("com.google.protobuf") version "0.8.14"

	// It is a necessary plug-in for an optimized Docker image configuration.
	// Reference Link: https://github.com/palantir/gradle-docker
	id("com.palantir.docker") version "0.22.1"
}

group = "ai.maum.cloud"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11
application.mainClassName = "ai.maum.cloud.admin.CloudAdminApplicationKt"

repositories {
	mavenLocal()
	mavenCentral()
	jcenter()
	google()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.security.oauth.boot:spring-security-oauth2-autoconfigure:2.1.4.RELEASE")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.mybatis.spring.boot:mybatis-spring-boot-starter:2.1.4")
	// oracle
	implementation("com.oracle:ojdbc6:12.1.0.1")
	runtimeOnly("com.h2database:h2")
	// kotlin serialization
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.0.0")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
//	testImplementation("org.springframework.security:spring-security-test")
}

//configurations {
//	all {
//		exclude(group = "org.springframework.boot", module = "spring-boot-starter-json")
//		exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
//	}
//}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.create<Copy>("unpack") {
	dependsOn(tasks.bootJar)
	from(zipTree(tasks.bootJar.get().outputs.files.singleFile))
	into("build/dependency")
}

docker {
	name = "maum-cloud-admin"
	copySpec.from(tasks.findByName("unpack")?.outputs).into("dependency")
	buildArgs(mapOf("DEPENDENCY" to "dependency"))
}
